﻿using System;
using NLog;

namespace Logger
{
    public interface INLogger
    {

        void WriteErrorMessage(string message);

        void WriteException(Exception exception, string message);

        void WriteException(Exception exception);
    }



    public class NLogger : INLogger
    {
        private static readonly NLog.Logger _logger = LogManager.GetCurrentClassLogger();

        public static INLogger Logger { get; } = new NLogger();

        public void WriteErrorMessage(string message)
        {
            _logger.Error(message);
        }

        public void WriteException(Exception exception, string message)
        {
            _logger.Error(message + Environment.NewLine + exception.Message + Environment.NewLine + exception.StackTrace);
        }

        public void WriteException(Exception exception)
        {
            _logger.Error(exception.Message + Environment.NewLine + exception.StackTrace);
        }
    }
}
