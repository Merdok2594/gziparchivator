﻿namespace Domain.Models
{
    public class FileParameters
    {
        public string SourceFile { get; set; }
        public string TargetFile { get; set; }
    }
}
