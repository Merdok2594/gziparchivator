﻿namespace Domain.Models
{
    public class Block
    {
        public int Id { get; }
        public byte[] Buffer { get; }
        public byte[] CompressedBuffer { get; }

        public Block(int id, byte[] buffer)
        {
            Id = id;
            Buffer = buffer;
            CompressedBuffer = new byte[0];
        }

        public Block(int id, byte[] buffer, byte[] compressedBuffer)
        {
            Id = id;
            Buffer = buffer;
            CompressedBuffer = compressedBuffer;
        }
    }
}
