﻿namespace Domain.Enums
{
    public enum ArchivatorState
    {
        NotStarted,
        Started,
        Successed,
        Failed,
        Aborted
    }
}
