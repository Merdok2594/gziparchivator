﻿namespace Domain.Enums
{
    public enum ArchivatorAction
    {
        Compress,
        Decompress
    }
}
