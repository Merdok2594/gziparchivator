﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;

using Domain.Enums;
using Domain.Models;

using Logger;

namespace BLL.Archivators
{
    public class ParallelGZipCompressor : ParallelArchivator
    {
        public ParallelGZipCompressor(FileParameters fileParameters) : base(fileParameters)
        {
        }

        public override void Execute()
        {
            _state = ArchivatorState.Started;

            Thread checkMemoryThread = new Thread(new ThreadStart(CheckMemory));
            checkMemoryThread.Start();

            Thread reader = new Thread(new ThreadStart(Read));
            reader.Start();

            for (int i = 0; i < _threads; i++)
            {
                _doneEvents[i] = new ManualResetEvent(false);
                Thread thread = new Thread(new ParameterizedThreadStart(Compress));
                thread.Start(i);
            }

            Thread writer = new Thread(new ThreadStart(Write));
            writer.Start();

            WaitHandle.WaitAll(_doneEvents);

            checkMemoryThread.Abort();

            if (_state == ArchivatorState.Started)
            {
                _state = ArchivatorState.Successed;
            }
        }

        private void Read()
        {
            try
            {
                using (FileStream fileToBeCompressed = new FileStream(_fileParameters.SourceFile, FileMode.Open))
                {
                    int bytesRead;
                    byte[] lastBuffer;

                    while (fileToBeCompressed.Position < fileToBeCompressed.Length && _state == ArchivatorState.Started)
                    {
                        if (fileToBeCompressed.Length - fileToBeCompressed.Position <= _blockSize)
                        {
                            bytesRead = (int)(fileToBeCompressed.Length - fileToBeCompressed.Position);
                        }

                        else
                        {
                            bytesRead = _blockSize;
                        }

                        if (_isStopped)
                        {
                            _readThread.WaitOne();
                        }

                        lastBuffer = new byte[bytesRead];
                        fileToBeCompressed.Read(lastBuffer, 0, bytesRead);
                        _queueReader.AddToQueueForCompressing(lastBuffer);
                    }

                    _queueReader.Stop();
                }
            }
            catch (OutOfMemoryException ex)
            {
                NLogger.Logger.WriteException(ex, "Error in read bytes block while compressing due to out of memory.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("There is no enough memory", ex));
            }
            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex, "Error in read bytes block while compressing.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("Something went wrong", ex));
            }
        }

        private void Compress(object threadNum)
        {
            try
            {
                while (_addedBlocksToQueueCount < _blocksCount && _state != ArchivatorState.Failed && _state != ArchivatorState.Aborted)
                {
                    Block block = _queueReader.TakeFromQueue();

                    if (block == null) continue;

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (GZipStream cs = new GZipStream(memoryStream, CompressionMode.Compress))
                        {
                            cs.Write(block.Buffer, 0, block.Buffer.Length);
                        }

                        byte[] compressedData = memoryStream.ToArray();
                        Block @out = new Block(block.Id, compressedData);
                        _queueWriter.AddToQueueForWriting(@out);
                    }
                }

                ManualResetEvent doneEvent = _doneEvents[(int)threadNum];
                doneEvent.Set();
            }
            catch (OutOfMemoryException ex)
            {
                NLogger.Logger.WriteException(ex, "Error in compress byte block due to out of memory.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("There is no enough memory", ex));
            }
            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex, "Error in compress byte block.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("Something went wrong", ex));
            }

        }

        private void Write()
        {
            try
            {
                using (FileStream fileCompressed = new FileStream(_fileParameters.TargetFile + ".gz", FileMode.Append))
                {
                    while (_addedBlocksToQueueCount < _blocksCount && _state != ArchivatorState.Failed && _state != ArchivatorState.Aborted)
                    {
                        Block block = _queueWriter.TakeFromQueue();

                        if (block == null) continue;

                        _addedBlocksToQueueCount++;

                        BitConverter.GetBytes(block.Buffer.Length).CopyTo(block.Buffer, 4);
                        fileCompressed.Write(block.Buffer, 0, block.Buffer.Length);
                    }
                }

            }
            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex, "Error in write bytes block while compressing.");
                _state = ArchivatorState.Failed;
            }

        }
    }
}
