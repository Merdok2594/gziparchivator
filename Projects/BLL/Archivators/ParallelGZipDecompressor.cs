﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;

using Domain.Enums;
using Domain.Models;

using Logger;

namespace BLL.Archivators
{
    public class ParallelGZipDecompressor : ParallelArchivator
    {
        private int _counter = 0;

        public ParallelGZipDecompressor(FileParameters fileParameters) : base(fileParameters)
        {
        }

        public override void Execute()
        {
            _state = ArchivatorState.Started;

            Thread checkMemoryThread = new Thread(new ThreadStart(CheckMemory));
            checkMemoryThread.Start();

            Thread reader = new Thread(new ThreadStart(Read));
            reader.Start();

            for (int i = 0; i < _threads; i++)
            {
                _doneEvents[i] = new ManualResetEvent(false);
                Thread thread = new Thread(new ParameterizedThreadStart(Decompress));
                thread.Start(i);
            }

            Thread writer = new Thread(new ThreadStart(Write));
            writer.Start();

            WaitHandle.WaitAll(_doneEvents);

            checkMemoryThread.Abort();
            _queueWriter.Stop();

            if (_state == ArchivatorState.Started)
            {
                _state = ArchivatorState.Successed;
            }
        }

        private void Read()
        {
            try
            {
                using (FileStream compressedFile = new FileStream(_fileParameters.SourceFile, FileMode.Open))
                {
                    while (compressedFile.Position < compressedFile.Length)
                    {
                        if (_isStopped)
                        {
                            _readThread.WaitOne();
                        }

                        byte[] lengthBuffer = new byte[8];
                        compressedFile.Read(lengthBuffer, 0, lengthBuffer.Length);
                        int blockLength = BitConverter.ToInt32(lengthBuffer, 4);
                        byte[] compressedData = new byte[blockLength];
                        lengthBuffer.CopyTo(compressedData, 0);

                        compressedFile.Read(compressedData, 8, blockLength - 8);
                        int dataSize = BitConverter.ToInt32(compressedData, blockLength - 4);
                        byte[] lastBuffer = new byte[dataSize];

                        Block _block = new Block(_counter, lastBuffer, compressedData);
                        _queueReader.AddToQueueForWriting(_block);
                        _counter++;
                    }
                    _queueReader.Stop();
                }
            }

            catch (OutOfMemoryException ex)
            {
                NLogger.Logger.WriteException(ex, "Error in read bytes block while decompressing due to out of memory.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("There is no enough memory", ex));
            }
            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex, "Error in read bytes block while decompressing.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("Something went wrong", ex));
            }
        }

        private void Decompress(object threadNum)
        {
            try
            {
                while (_state != ArchivatorState.Failed && _state != ArchivatorState.Aborted)
                {
                    Block block = _queueReader.TakeFromQueue();

                    if (block == null) break;

                    using (MemoryStream ms = new MemoryStream(block.CompressedBuffer))
                    {
                        using (GZipStream _gz = new GZipStream(ms, CompressionMode.Decompress))
                        {
                            _gz.Read(block.Buffer, 0, block.Buffer.Length);
                            byte[] decompressedData = block.Buffer.ToArray();
                            Block @out = new Block(block.Id, decompressedData);
                            _queueWriter.AddToQueueForWriting(@out);
                        }
                    }
                }

                ManualResetEvent doneEvent = _doneEvents[(int)threadNum];
                doneEvent.Set();
            }

            catch (OutOfMemoryException ex)
            {
                NLogger.Logger.WriteException(ex, "Error in decompress byte block due to out of memory.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("There is no enough memory", ex));
            }
            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex, "Error in decompress byte block.");
                _state = ArchivatorState.Failed;
                RaiseExceptionThrownEvent(new Events.ErrorEventArgs("Something went wrong", ex));
            }
        }

        private void Write()
        {
            try
            {
                using (FileStream decompressedFile = new FileStream(_fileParameters.TargetFile, FileMode.Append))
                {
                    while (_state != ArchivatorState.Failed && _state != ArchivatorState.Aborted)
                    {
                        Block block = _queueWriter.TakeFromQueue();
                        if (block == null) return;

                        decompressedFile.Write(block.Buffer, 0, block.Buffer.Length);
                    }
                }
            }

            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex, "Error in write bytes block while decompressing.");
                _state = ArchivatorState.Failed;
            }
        }
    }
}
