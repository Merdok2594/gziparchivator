﻿using Domain.Enums;
using Domain.Models;

namespace BLL.Archivators
{
    public abstract class ArchivatorCreator
    {
        protected InputParameters _input;

        protected ArchivatorCreator(InputParameters input)
        {
            _input = input;
        }

        public abstract Archivator Create();

        public static ArchivatorCreator GetCreator(InputParameters input)
        {
            if (input.Action == ArchivatorAction.Compress) return new ParallelGZipCompressorCreator(input);

            return new ParallelGZipDecompressorCreator(input);
        }
    }

    public class ParallelGZipCompressorCreator : ArchivatorCreator
    {
        public ParallelGZipCompressorCreator(InputParameters input) : base(input)
        {
        }

        public override Archivator Create()
        {
            return new ParallelGZipCompressor(new FileParameters
            {
                SourceFile = _input.InputFile,
                TargetFile = _input.OutputFile
            });
        }
    }

    public class GZipCompressorCreator : ArchivatorCreator
    {
        public GZipCompressorCreator(InputParameters input) : base(input)
        {
        }

        public override Archivator Create()
        {
            return new GZipCompressor(new FileParameters
            {
                SourceFile = _input.InputFile,
                TargetFile = _input.OutputFile
            });
        }
    }

    public class ParallelGZipDecompressorCreator : ArchivatorCreator
    {
        public ParallelGZipDecompressorCreator(InputParameters input) : base(input)
        {
        }

        public override Archivator Create()
        {
            return new ParallelGZipDecompressor(new FileParameters
            {
                SourceFile = _input.InputFile,
                TargetFile = _input.OutputFile
            });
        }
    }
}
