﻿using System;
using System.IO;
using System.Threading;

using Microsoft.VisualBasic.Devices;

using BLL.Managers;

using Domain.Models;

namespace BLL.Archivators
{
    public abstract class ParallelArchivator : Archivator
    {
        private readonly ComputerInfo _computerInfo = new ComputerInfo();

        protected static int _threads = Environment.ProcessorCount;
        protected readonly int _limitMemory;
        protected readonly int _mbMult = 1024 * 1024;
        protected bool _isStopped = false;

        protected int _blocksCount;
        protected int _addedBlocksToQueueCount = 0;
        protected readonly int _blockSize = 10_000_000;
        protected QueueManager _queueReader = new QueueManager();
        protected QueueManager _queueWriter = new QueueManager();
        protected AutoResetEvent _readThread = new AutoResetEvent(false);
        protected ManualResetEvent[] _doneEvents = new ManualResetEvent[_threads];

        protected ParallelArchivator(FileParameters fileParameters) : base(fileParameters)
        {
            var fileInfo = new FileInfo(fileParameters.SourceFile);
            _blocksCount = (int)Math.Ceiling(1.0f * fileInfo.Length / _blockSize);
            _limitMemory = (int)(new ComputerInfo().TotalPhysicalMemory / (ulong)_mbMult / 8);
        }

        protected void CheckMemory()
        {
            while (true)
            {
                if (GetTotalRAMByApp() > 80 * GetAvailableRAM() / 100)
                {
                    _isStopped = true;
                }
                else
                {
                    if (_isStopped)
                    {
                        _isStopped = false;
                        _readThread.Set();
                    }
                }
            }
        }

        private int GetAvailableRAM()
        {
            return (int)(_computerInfo.AvailablePhysicalMemory / (ulong)_mbMult);
        }

        private int GetTotalRAMByApp()
        {
            return (int)(GC.GetTotalMemory(true) / _mbMult);
        }
    }
}
