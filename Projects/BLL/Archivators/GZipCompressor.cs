﻿using System.IO;
using System.IO.Compression;

using Domain.Enums;
using Domain.Models;

namespace BLL.Archivators
{
    public class GZipCompressor : Archivator
    {
        public GZipCompressor(FileParameters fileParameters) : base(fileParameters)
        {
        }

        public override void Execute()
        {
            _state = ArchivatorState.Started;
            Compress();
            _state = ArchivatorState.Successed;
        }

        private void Compress()
        {
            using (FileStream sourceStream = new FileStream(_fileParameters.SourceFile, FileMode.OpenOrCreate))
            {
                using (FileStream targetStream = File.Create(_fileParameters.TargetFile + ".gz"))
                {
                    using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                    {
                        sourceStream.CopyTo(compressionStream);
                    }
                }
            }
        }
    }
}
