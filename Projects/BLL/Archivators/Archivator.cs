﻿using BLL.Events;

using Domain.Enums;
using Domain.Models;

using System;

namespace BLL.Archivators
{
    public abstract class Archivator
    {
        protected FileParameters _fileParameters;
        protected ArchivatorState _state = ArchivatorState.NotStarted;

        public event Action<object, ErrorEventArgs> ExceptionThrown;

        protected Archivator(FileParameters fileParameters)
        {
            _fileParameters = fileParameters;

        }

        public abstract void Execute();

        public void Abort()
        {
            _state = ArchivatorState.Aborted;
        }

        public int GetResult()
        {
            if (_state == ArchivatorState.Failed) return 1;

            return 0;
        }

        protected void RaiseExceptionThrownEvent(ErrorEventArgs args)
        {
            ExceptionThrown?.Invoke(this, args);
        }
    }
}
