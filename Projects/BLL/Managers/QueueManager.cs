﻿using System;
using System.Collections.Generic;
using System.Threading;

using Domain.Models;

namespace BLL.Managers
{
    public class QueueManager
    {
        private readonly object _locker = new object();
        private Queue<Block> _queue = new Queue<Block>();
        private bool _isDead = false;
        private int _blockId = 0;

        public void AddToQueueForWriting(Block block)
        {
            var id = block.Id;

            lock (_locker)
            {
                if (_isDead) throw new InvalidOperationException("Queue already stopped");

                while (id != _blockId)
                {
                    Monitor.Wait(_locker);
                }

                _queue.Enqueue(block);
                _blockId++;
                Monitor.PulseAll(_locker);
            }
        }

        public void AddToQueueForCompressing(byte[] buffer)
        {
            lock (_locker)
            {
                if (_isDead)
                    throw new InvalidOperationException("Queue already stopped");

                Block block = new Block(_blockId, buffer);
                _queue.Enqueue(block);
                _blockId++;
                Monitor.PulseAll(_locker);
            }
        }

        public Block TakeFromQueue()
        {
            lock (_locker)
            {
                while (_queue.Count == 0 && !_isDead)
                {
                    Monitor.Wait(_locker);
                }

                if (_queue.Count == 0)
                {
                    return null;
                }

                return _queue.Dequeue();
            }
        }

        public void Stop()
        {
            lock (_locker)
            {
                _isDead = true;
                Monitor.PulseAll(_locker);
            }
        }
    }
}
