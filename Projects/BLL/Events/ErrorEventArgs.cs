﻿using System;

namespace BLL.Events
{
    public class ErrorEventArgs
    {
        public ErrorEventArgs(string message, Exception exception)
        {
            Message = message;
            Exception = exception;
        }

        public string Message { get; }
        public Exception Exception { get; }
    }
}
