﻿using System;

using BLL.Archivators;

using Domain.Models;

using Logger;

namespace GZipArchivator
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelKeyPress);

            try
            {
                args = new string[3];
                args[0] = @"compress";
                args[1] = @"C:\Users\Merdok\Desktop\testFile\testFile.iso";
                args[2] = @"C:\Users\Merdok\Desktop\testFile\CompressedTestFile.iso";

                var input = new InputParameters(args);

                var archivatorCreator = ArchivatorCreator.GetCreator(input);
                var archivator = archivatorCreator.Create();
                archivator.ExceptionThrown += Archivator_ExceptionThrown;

                Console.WriteLine($"Action: {args[0]}");
                Console.WriteLine($"Source file: {args[1]}");
                Console.WriteLine($"Target file: {args[2]}");

                Console.WriteLine("If it is right press any key.");
                Console.WriteLine("If it is wrong press CTRL + C.\n\n");

                Console.ReadKey();

                Console.WriteLine("Process starting ...");
                archivator.Execute();
                Console.WriteLine("Process completed");

                Console.ReadKey();

                return archivator.GetResult();
            }

            catch (Exception ex)
            {
                NLogger.Logger.WriteException(ex);
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return 1;
            }
        }

        private static void Archivator_ExceptionThrown(object sender, BLL.Events.ErrorEventArgs args)
        {
            Console.WriteLine(args.Message);
        }

        static void CancelKeyPress(object sender, ConsoleCancelEventArgs _args)
        {
            if (_args.SpecialKey == ConsoleSpecialKey.ControlC)
            {
                Environment.Exit(0);
            }
        }
    }
}
